package test;

import static org.junit.Assert.*;
import org.junit.Test;

import projetBN.Case;
import projetBN.Bateau;

public class TestCase {

	/**
	 * Test constructeur
	 */
	@Test
	public void testConstructeur() {
		Case c = new Case(5, 5);

		assertEquals("L'abscisse de la case devrait �tre 5", 5, c.getX());
		assertEquals("L'ordonn�e de la case devrait �tre 5", 5, c.getY());
	}

	/**
	 * Test methode etre touche
	 * 
	 * cas case non occupee
	 */
	@Test
	public void testEtreToucheVide() {
		Case c = new Case(5, 5);

		c.etreTouche();

		assertFalse("La case ne doit toujours pas �tre occup�e", c.getOccupe());
	}

	/**
	 * Cas case occupee
	 */
	@Test
	public void testEtreToucheOccupe() {
		Case c = new Case(5, 5);
		c.setOccupe(true);

		c.etreTouche();

		assertFalse("La case ne doit plus �tre occup�e", c.getOccupe());
	}

	/**
	 * Test accesseur abscisse
	 */
	@Test
	public void testGetX() {
		Case c = new Case(5, 5);

		assertEquals("L'abscisse de la case doit �tre 5", 5, c.getX());
	}

	/**
	 * Test accesseur ordonee
	 */
	@Test
	public void testGetY() {
		Case c = new Case(5, 5);

		assertEquals("L'ordonn�e de la case doit �tre 5", 5, c.getY());
	}

	/**
	 * Test accesseur valeur occupe
	 */
	@Test
	public void testGetOccupe() {
		Case c1 = new Case(5, 5);
		Case c2 = new Case(0, 0);

		c2.setOccupe(true);

		assertFalse("La case ne devrait pas etre occup�e", c1.getOccupe());
		assertTrue("La case devrait �tre occup�e", c2.getOccupe());
	}

	/**
	 * Test accesseur valeur bateau
	 * 
	 * Cas case vide
	 */
	@Test
	public void testGetBateauVide() {
		Case c = new Case(5, 5);

		assertEquals("La case c ne devrait pas contenir de bateau", null, c.getBateau());
	}

	/**
	 * Cas case occupee
	 */
	@Test
	public void testGetBateauOccupe() {
		Case c = new Case(5, 5);
		Bateau b = new Bateau("B1", 2, "vertical");

		c.setBateau(b);

		assertEquals("La case devrait contenir un bateau", b, c.getBateau());
	}

	/**
	 * Test setter valeur occupe
	 */
	@Test
	public void testSetOccupe() {
		Case c = new Case(5, 5);

		c.setOccupe(true);

		assertTrue("La case dervait �tre occup�e", c.getOccupe());
	}

	/**
	 * Test setter valeur bateau
	 */
	@Test
	public void testSetBateau() {
		Case c = new Case(5, 5);
		Bateau b = new Bateau("B1", 2, "vertical");

		c.setBateau(b);
		
		assertEquals("La case devrait contenir un bateau", b, c.getBateau());
	}
}
