package projetBN;

/**
 * Classe qui modelise le joueur
 */
public class Joueur {

	/**
	 * Attributs de la classe
	 */
	private String nom;
	private Bateau[] bateaux;
	private int taillemax = 0;
	private Grille grille;

	/**
	 * Constructeur principal de la classe
	 * 
	 * @param nomjoueur nom du joueur
	 * @param lisbat    liste des bateaux possedes par le joueur
	 * @param g         grille appartenant au joueur
	 */
	public Joueur(String nomjoueur, Bateau[] lisbat, Grille g) {

		nom = nomjoueur;
		bateaux = lisbat;
		grille = g;
		for (int i = 0; i < bateaux.length; i++) {
			taillemax += bateaux[i].getTaille();
		}
	}

	/**
	 * Methode permettant d'attaquer un autre joueur
	 * 
	 * @param j joueur a attaquer
	 * @param x abscisse de la case sur la grille du joueur attaque
	 * @param y ordonnee de la case sur la grille du joueur attaque
	 * @return true si l'attaque a touche un bateau ennemi
	 */
	public boolean attaquer(Joueur j, int x, int y) {
		boolean res = false;
		try {
			res = j.getGrille().subirAttaque(x, y);
		} catch (IndexOutOfBoundsException e) {
			System.out.println(e);
		}
		return res;
	}

	/**
	 * Accesseur du nom du joueur
	 * 
	 * @return nom du joueur
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Renvoie le bateu a l'indice entre en parametre dans la liste des bateaux du
	 * joueur
	 * 
	 * @param i indice du bateau
	 * @return bateau d'indice i
	 */
	public Bateau getBateaux(int i) {
		return bateaux[i];
	}

	/**
	 * Accesseur de la grille possede par le joueur
	 * 
	 * @return grille appartenant au joueur
	 */
	public Grille getGrille() {
		return grille;
	}

	/**
	 * Accesseur de la taille cumul�e des bateaux du joueur
	 * 
	 * @return taille cumul�e des bateaux du joueur
	 */
	public int getTaillemax() {
		return taillemax;
	}

}
