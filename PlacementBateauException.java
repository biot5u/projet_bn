package projetBN;

/**
 * Classe qui modelise l'exception lors d'un placement de bateau impossible
 *
 */
public class PlacementBateauException extends Exception {

	/**
	 * Constructeur de l'exception
	 * 
	 * @param message message a afficher lors de la levee de l'exception
	 */
	public PlacementBateauException(String message) {
		super(message);
	}
}
