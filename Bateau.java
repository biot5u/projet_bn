package projetBN;

import java.util.*;

/**
 * Classe qui modelise les bateaux
 */
public class Bateau {

	/**
	 * Attributs de la classe
	 */
	private String nom;
	private int taille;
	private String direction;
	private ArrayList<Case> position;

	/**
	 * Constructeur principal de la classe
	 * 
	 * @param t   taille du bateau
	 * @param dir orientation du bateau
	 */
	public Bateau(String s, int t, String dir) {
		
		nom = s;
		
		if (t <= 5 && t > 0) {
			this.taille = t;
		} else {
			this.taille = 2;
		}

		if (dir != "horizontal" && dir != "vertical") {
			this.direction = "vertical";
		} else {
			this.direction = dir;
		}
	}

	/**
	 * Accesseur de la position du bateau
	 * 
	 * @return pemiere case sur laquelle le bateau est situe
	 */
	public ArrayList<Case> getPosition() {
		return this.position;
	}

	/**
	 * Accesseur de la taille du bateau
	 * 
	 * @return taille du bateau
	 */
	public int getTaille() {
		return this.taille;
	}

	/**
	 * Accesseur de la direction du bateau
	 * 
	 * @return direction du bateau
	 */
	public String getDirection() {
		return this.direction;
	}
	
	/**
	 * Accesseur du nom du bateau
	 * 
	 * @return nom du bateau
	 */
	public String getNom() {
		return nom;
	}
}
