package test;

import static org.junit.Assert.*;
import org.junit.Test;

import projetBN.Joueur;
import projetBN.Bateau;
import projetBN.Grille;

public class TestJoueur {

	/**
	 * Test constructeur
	 */
	@Test
	public void testConstructeur() {
		Grille g = new Grille(10, 10);
		Bateau[] bat = new Bateau[] { new Bateau("B1", 2, ""), new Bateau("B2", 3, "") };
		Joueur j = new Joueur("Joueur 1", bat, g);

		assertEquals("Le nom du joueur devrair �tre Joueur 1", "Joueur 1", j.getNom());
		assertEquals("La grille du joueur devrait �tre g", g, j.getGrille());
	}

	/**
	 * Test methode attaquer
	 * 
	 * Cas abscisse trop grande
	 */
	@Test(expected = IndexOutOfBoundsException.class)
	public void testAttaquerAbscisseTropGrande() throws IndexOutOfBoundsException {
		Grille g = new Grille(10, 10);
		Bateau[] bat = new Bateau[] { new Bateau("B1", 2, ""), new Bateau("B2", 3, "") };
		Joueur j = new Joueur("Joueur 1", bat, g);

		j.attaquer(j, 10, 5);
	}

	/**
	 * Cas ordonnee trop grande
	 */
	@Test(expected = IndexOutOfBoundsException.class)
	public void testAttaquerOrdonneeTropGrande() throws IndexOutOfBoundsException {
		Grille g = new Grille(10, 10);
		Bateau[] bat = new Bateau[] { new Bateau("B1", 2, ""), new Bateau("B2", 3, "") };
		Joueur j = new Joueur("Joueur 1", bat, g);

		j.attaquer(j, 5, 10);
	}

	/**
	 * Cas abscisse trop petite
	 */
	@Test(expected = IndexOutOfBoundsException.class)
	public void testAttaquerAbscisseTropPetite() throws IndexOutOfBoundsException {
		Grille g = new Grille(10, 10);
		Bateau[] bat = new Bateau[] { new Bateau("B1", 2, ""), new Bateau("B2", 3, "") };
		Joueur j = new Joueur("Joueur 1", bat, g);

		j.attaquer(j, -1, 5);
	}

	/**
	 * Cas ordonnee trop petite
	 */
	@Test(expected = IndexOutOfBoundsException.class)
	public void testAttaquerOrdonneeTropPetite() throws IndexOutOfBoundsException {
		Grille g = new Grille(10, 10);
		Bateau[] bat = new Bateau[] { new Bateau("B1", 2, ""), new Bateau("B2", 3, "") };
		Joueur j = new Joueur("Joueur 1", bat, g);

		j.attaquer(j, 5, -1);
	}

	/**
	 * Cas case vide
	 */
	@Test
	public void testAttaquerCaseVide() {
		Grille g = new Grille(10, 10);
		Bateau[] bat = new Bateau[] { new Bateau("B1", 2, ""), new Bateau("B2", 3, "") };
		Joueur j = new Joueur("Joueur 1", bat, g);

		boolean res = j.attaquer(j, 5, 5);

		assertFalse("L'attaque ne devrait pas toucher", res);
	}

	/**
	 * Cas case occupee
	 */
	@Test
	public void testAttaquerCaseOccupee() {
		Grille g = new Grille(10, 10);
		Bateau[] bat = new Bateau[] { new Bateau("B1", 2, ""), new Bateau("B2", 3, "") };
		Joueur j = new Joueur("Joueur 1", bat, g);

		g.placerBateau(bat[1], 5, 5);
		boolean res = j.attaquer(j, 5, 5);

		assertTrue("L'attaque devrait avoir touch�", res);
	}

	/**
	 * Test accesseur du nom du joueur
	 */
	@Test
	public void testGetNom() {
		Grille g = new Grille(10, 10);
		Bateau[] bat = new Bateau[] { new Bateau("B1", 2, ""), new Bateau("B2", 3, "") };
		Joueur j = new Joueur("Joueur 1", bat, g);

		assertEquals("Le nom devrait �tre Joueur 1", "Joueur 1", j.getNom());
	}

	/**
	 * Test accesseur des bateaux du joueur
	 */
	@Test
	public void testGetBateaux() {
		Grille g = new Grille(10, 10);
		Bateau b1 = new Bateau("B1", 2, "");
		Bateau b2 = new Bateau("B2", 3, "");
		Bateau[] bat = new Bateau[] { b1, b2 };
		Joueur j = new Joueur("Joueur 1", bat, g);

		assertEquals("Le premier bateau devrait �tre b1", b1, j.getBateaux(0));
		assertEquals("Le deuxi�me bateau devrait �tre b2", b2, j.getBateaux(1));
	}

	/**
	 * Test accesseur de le grille du joueur
	 */
	@Test
	public void testGetGrille() {
		Grille g = new Grille(10, 10);
		Bateau[] bat = new Bateau[] { new Bateau("B1", 2, ""), new Bateau("B2", 3, "") };
		Joueur j = new Joueur("Joueur 1", bat, g);

		assertEquals("La grille devrait �tre g", g, j.getGrille());
	}

	/**
	 * Test accesseur de la taille cumul�e des bateaux du joueur
	 */
	@Test
	public void testGetTaillemax() {
		Grille g = new Grille(10, 10);
		Bateau[] bat = new Bateau[] { new Bateau("B1", 2, ""), new Bateau("B2", 3, "") };
		Joueur j = new Joueur("Joueur 1", bat, g);

		assertEquals("La taille cumul�e devrait �tre 5", 5, j.getTaillemax());
	}
}
