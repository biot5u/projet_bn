package test;

import static org.junit.Assert.*;
import org.junit.Test;

import projetBN.Bateau;

public class TestBateau {

	/**
	 * Test contructeur de la classe
	 * 
	 * Cas normal
	 */
	@Test
	public void testConstructeurNormal() {
		Bateau b = new Bateau("B1", 5, "horizontal");

		assertEquals("Le nom du bateau devarit �tre B1", "B1", b.getNom());
		assertEquals("La taille du bateau devrait �tre 5", 5, b.getTaille());
		assertEquals("La direction du bateau devrait �tre horizontale", "horizontale", b.getDirection());
	}

	/**
	 * Cas direction et taille incorrects
	 */
	@Test
	public void testConstructeurIncorrect() {
		Bateau b = new Bateau("B1", 10, "");

		assertEquals("La taille du bateau devrait �tre 2", 2, b.getTaille());
		assertEquals("La direction du bateau devrait �tre verticale", "vertical", b.getDirection());
	}

	/**
	 * Test accesseur de la taille du bateau
	 */
	@Test
	public void testGetTaille() {
		Bateau b = new Bateau("B1", 5, "horizontal");

		assertEquals("La taille du bateau devrait �tre 5", 5, b.getTaille());
	}

	/**
	 * Test accesseur direction du bateau
	 */
	@Test
	public void testGetDirection() {
		Bateau b = new Bateau("B1", 5, "horizontal");

		assertEquals("La direction du bateau devrait �tre horizontale", "horizontale", b.getDirection());
	}

	/**
	 * Test accesseur du nom du bateau
	 */
	@Test
	public void testGetNom() {
		Bateau b = new Bateau("B1", 5, "horizontal");

		assertEquals("Le nom du bateau devrait �tre B1", "B1", b.getNom());
	}
}
