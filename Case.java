package projetBN;

/**
 * Classe qui modelise une case d'une grille
 *
 */
public class Case {
	/**
	 * Attributs de la classe
	 */
	private int x, y;
	private boolean occupe;
	private Bateau bateau;

	/**
	 * Constructeur principal de la classe
	 * 
	 * @param x abscissee de la case
	 * @param y ordonnee de la case
	 */
	public Case(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Methode declenchee losqu'une case attaquee est occupee par un bateau
	 */
	public void etreTouche() {
		this.occupe = false;
	}

	/**
	 * Methode toString de la classe
	 */
	public String toString() {
		if (occupe) {
			return "x";
		} else {
			return ".";
		}
	}

	/**
	 * Accceseur de l'abscisse de la case
	 * 
	 * @return abscisse de la case
	 */
	public int getX() {
		return x;
	}

	/**
	 * Accesseur de l'ordonne de la case
	 * 
	 * @return ordonnee de la case
	 */
	public int getY() {
		return y;
	}

	/**
	 * Accesseur 
	 * 
	 * @return
	 */
	public boolean getOccupe() {
		return this.occupe;
	}

	/**
	 * Setter
	 * 
	 * @param val valeur a modifier
	 */
	public void setOccupe(boolean val) {
		occupe = val;
	}
	
	/**
	 * Accesseur du bateau situ� sur la case
	 * 
	 * @return bateau situ� sur la case
	 */
	public Bateau getBateau() {
		return bateau;
	}
	
	/**
	 * Setter du bateau situ� sur la case
	 * 
	 * @param bat bateau situ� sur la case
	 */
	public void setBateau(Bateau bat) {
		bateau = bat;
	}
}
