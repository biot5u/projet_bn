package projetBN;

import java.util.Scanner;

/**
 * Classe pricipale
 */
public class Jeu {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Quel est la taille de la grille en largeur ?");
		int x = sc.nextInt();
		System.out.println("Quel est la taille de la grille en hauteur ?");
		int y = sc.nextInt();
		Grille grillej1 = new Grille(x, y);
		Grille grillej2 = new Grille(x, y);
		Bateau torpilleur = new Bateau("Torpilleur", 2, "horizontal");
		Bateau sousmarin = new Bateau("Sous-marin", 3, "horizontal");
		Bateau contretorpilleur = new Bateau("Contre-torpilleur", 3, "vertical");
		Bateau croiseur = new Bateau("Croiseur", 4, "vertical");
		Bateau porteavion = new Bateau("Porte-avion", 5, "horizontal");
		Bateau bateauxj1[] = { torpilleur, sousmarin, contretorpilleur, croiseur, porteavion };
		Bateau bateauxj2[] = { torpilleur, sousmarin, contretorpilleur, croiseur, porteavion };
		Joueur j1 = new Joueur("Joueur 1", bateauxj1, grillej1);
		Joueur j2 = new Joueur("Joueur 2", bateauxj2, grillej2);

		grillej1.placerBateau(torpilleur, 0, 0);
		grillej1.placerBateau(sousmarin, 6, 9);
		grillej1.placerBateau(contretorpilleur, 9, 3);
		grillej1.placerBateau(croiseur, 5, 5);
		grillej1.placerBateau(porteavion, 0, 9);
		
		grillej2.placerBateau(torpilleur, 6, 2);
		grillej1.placerBateau(sousmarin, 1, 1);
		grillej1.placerBateau(contretorpilleur, 9, 0);
		grillej1.placerBateau(croiseur, 9, 5);
		grillej1.placerBateau(porteavion, 0, 9);

		int taillemaxj1 = j1.getTaillemax(), taillemaxj2 = j2.getTaillemax();
		int nbtours = 1;

		System.out.println("1er tour");
		while (taillemaxj1 > 0 && taillemaxj2 > 0) {
			System.out.println("Au tour de " + j1.getNom());
			System.out.println("O� voulez-vous attaquer ?");
			System.out.println("Entrez l'abscisse :");
			int x1 = sc.nextInt();
			System.out.println("Entrez l'ordonn�e :");
			int y1 = sc.nextInt();
			boolean res = j1.attaquer(j2, x1, y1);
			if (res == true) {
				taillemaxj2--;
				if (taillemaxj2 == 0) {
					break;
				}
			}
			System.out.println("Au tour de " + j2.getNom());
			System.out.println("O� voulez-vous attaquer ?");
			System.out.println("Entrez l'abscisse :");
			int x2 = sc.nextInt();
			System.out.println("Entrez l'ordonn�e :");
			int y2 = sc.nextInt();
			boolean res2 = j2.attaquer(j1, x2, y2);
			if (res2 == true) {
				taillemaxj1--;
				if (taillemaxj1 == 0) {
					break;
				}
			}
			System.out.println("Voulez-vous continuer ? Oui=1/Non=0");
			int ans = sc.nextInt();
			if (ans == 0) {
				break;
			}
			nbtours++;
			System.out.println(nbtours + "�me tour");
		}
		if (taillemaxj2 == 0) {
			System.out.println(j1.getNom() + " a gagn� !, la partie a dur�e " + nbtours + " tours.");
		} else if (taillemaxj1 == 0) {
			System.out.println(j2.getNom() + " a gagn� !, la partie a dur�e " + nbtours + " tours.");
		} else {
			System.out.println("Peut-�tre une autre fois...");
		}

		sc.close();
	}
}