package projetBN;

import java.util.*;

/**
 * Classe qui modelise la grille de jeu
 */
public class Grille {

	/**
	 * Attributs de la classe
	 */
	private int hauteur;
	private int largeur;
	ArrayList<Case> grille;

	 /**
	 * Constructeur principal de la classe
	 * 
	 * @param l largeur de la grille
	 * @param h hauteur de la grille
	 */
	public Grille(int l, int h) {
		largeur = l;
		hauteur = h;
		grille = new ArrayList<Case>();
		for (int i = 0; i < largeur; i++) {
			for (int j = 0; j < hauteur; j++) {
				grille.add(new Case(j, i));
			}
		}
	}

	/**
	 * Methode qui retourne la case correspondante aux coordonnees en parametre de
	 * la grille
	 * 
	 * @param x abscisse de la case
	 * @param y ordonnee de la case
	 * @return case de la grille
	 */
	public Case getCase(int x, int y) {
		int index = y * hauteur + x;
		return grille.get(index);
	}

	/**
	 * Methode toString de la classe
	 */
	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < largeur; i++) {
			for (int j = 0; j < hauteur; j++) {
				s += getCase(j, i).toString();
			}
			s += "\n";
		}
		return s;
	}

	/**
	 * Methode permettant de placer un bateau sur la grille
	 * 
	 * @param bat bateau a placer
	 * @param x   abscisse de la case ou placer le bateau
	 * @param y   ordonnee de la case ou placer le bateau
	 */
	public void placerBateau(Bateau bat, int x, int y) {
		try {
			if (bat.getDirection() == "vertical" && y + bat.getTaille() <= largeur) {
				for (int i = 0; i < bat.getTaille(); i++) {
					getCase(x, y + i).setOccupe(true);
					getCase(x, y + i).setBateau(bat);
				}
			} else if (bat.getDirection() == "horizontal" && x + bat.getTaille() <= hauteur) {
				for (int i = 0; i < bat.getTaille(); i++) {
					getCase(x + i, y).setOccupe(true);
					getCase(x + i, y).setBateau(bat);
				}
			} else {
				throw new PlacementBateauException("Placement du bateau impossible");
			}
		} catch (PlacementBateauException e) {
			System.out.println(e);
		}
	}

	/**
	 * Methode declenchee lorsque la grille subit une attaque
	 * 
	 * @param x abscisse de la case attaquee
	 * @param y ordonnee de la case attaquee
	 * @return true si un bateau se trouvait sur la case
	 */
	public boolean subirAttaque(int x, int y) {
		if (this.getCase(x, y).getOccupe()) {
			this.getCase(x, y).etreTouche();
			System.out.println(this.getCase(x, y).getBateau().getNom() + " ennemi touch� !");
			this.getCase(x, y).setBateau(null);
			return true;
		} else {
			System.out.println("Manqu� !");
			return false;
		}
	}

	/**
	 * Accesseur de la hauteur de la grille
	 * 
	 * @return hauteur de la grille
	 */
	public int getHauteur() {
		return hauteur;
	}

	/**
	 * Accesseur de la largeur de la grille
	 * 
	 * @return largeur de la grille
	 */
	public int getLargeur() {
		return largeur;
	}
}
