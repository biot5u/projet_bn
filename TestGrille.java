package test;

import static org.junit.Assert.*;
import org.junit.Test;

import projetBN.Bateau;
import projetBN.Grille;
import projetBN.PlacementBateauException;

/**
 * Classe de tests de la classe Grille
 */
public class TestGrille {

	/**
	 * Test constructeur grille
	 */
	@Test
	public void testConstucteur() {
		Grille g = new Grille(10, 10);

		assertEquals("La hauteur devrait �tre de 10", 10, g.getHauteur());
		assertEquals("La largeur devrait �tre de 10", 10, g.getLargeur());

	}

	/**
	 * Test placement bateau
	 * 
	 * Cas exception levee
	 */
	@Test(expected = PlacementBateauException.class)
	public void testPlacementBateauException() throws PlacementBateauException {
		Grille g = new Grille(10, 10);
		Bateau b = new Bateau("B1", 3, "vertical");

		g.placerBateau(b, 8, 0);
	}

	/**
	 * Cas bateau horizontal
	 */
	@Test
	public void testPlacementBateauHorizontal() {
		Grille g = new Grille(10, 10);
		Bateau b = new Bateau("B1", 3, "horizontal");

		g.placerBateau(b, 5, 5);

		assertEquals("La case (5,5) devrait etre occupee", true, g.getCase(5, 5).getOccupe());
		assertEquals("La case (6,5) devrait etre occupee", true, g.getCase(6, 5).getOccupe());
		assertEquals("La case (7,5) devrait etre occupee", true, g.getCase(7, 5).getOccupe());
	}

	/**
	 * Cas bateau vertical
	 */
	@Test
	public void testPlacementBateauVertical() {
		Grille g = new Grille(10, 10);
		Bateau b = new Bateau("B1", 3, "vertical");

		g.placerBateau(b, 5, 5);

		assertEquals("La case (5,5) devrait etre occupee", true, g.getCase(5, 5).getOccupe());
		assertEquals("La case (6,5) devrait etre occupee", true, g.getCase(5, 6).getOccupe());
		assertEquals("La case (7,5) devrait etre occupee", true, g.getCase(5, 7).getOccupe());

		assertEquals("La case (5,5) devrait etre occupee par le bateau B1", "B1", g.getCase(5, 5).getBateau().getNom());
		assertEquals("La case (5,6) devrait etre occupee par le bateau B1", "B1", g.getCase(5, 6).getBateau().getNom());
		assertEquals("La case (5,7) devrait etre occupee par le bateau B1", "B1", g.getCase(5, 7).getBateau().getNom());
	}

	/**
	 * Test methode subir attaque
	 * 
	 * Cas case vide
	 */
	@Test
	public void testSubirAttaqueVide() {
		Grille g = new Grille(10, 10);
		Bateau b = new Bateau("B1", 3, "vertical");

		g.placerBateau(b, 5, 5);
		boolean res = g.subirAttaque(0, 0);

		assertEquals("La case ne doit pas �tre touch�e", false, res);
	}

	/**
	 * Cas case occupee
	 */
	@Test
	public void testSubirAttaqueOccuppe() {
		Grille g = new Grille(10, 10);
		Bateau b = new Bateau("B1", 3, "vertical");

		g.placerBateau(b, 5, 5);
		boolean res = g.subirAttaque(5, 5);

		assertEquals("La case doit �tre touch�e", true, res);
		assertEquals("La case ne doit plus �tre occup�e", false, g.getCase(5, 5).getOccupe());
		assertEquals("Il ne doit plus y avoir de bateau sur la case", null, g.getCase(5, 5).getBateau());
	}
	
	/**
	 * Test accesseur hauteur grille
	 */
	@Test
	public void testGetHauteur() {
		Grille g = new Grille(10, 10);
		
		assertEquals("La hauteur devrait �tre de 10", 10, g.getHauteur());
	}
	
	/**
	 * Test accesseur hauteur grille
	 */
	@Test
	public void testGetLargeur() {
		Grille g = new Grille(10, 10);
		
		assertEquals("La largeur devrait �tre de 10", 10, g.getLargeur());
	}
}
